href
====

The URL of the subscription list.

The URL in ``href`` represents the last URL requested by listparser. For instance, if a subscription list at "olddomain.com" has been moved to "newdomain.com", ``href`` will contain the subscription list's address at "newdomain.com".

..  seealso:: :doc:`status`

Welcome to listparser's documentation!
======================================

listparser is a Python module that parses subscription lists (also called reading lists) and returns all of the feeds and subscription lists that it finds. It supports OPML, RDF+FOAF, and the iGoogle exported settings format.

listparser has been tested and runs on Python 2.4 and up, Python 3.0 and up, Jython 2.5.2 and up, PyPy 1.8.0, and IronPython 2.6.2.

Contents
--------

..  toctree::
    :maxdepth: 2

    getting-started
    http-features
    detection-algorithm
    reference/objects


Reference
---------

..  toctree::
    :maxdepth: 2

    reference/parse
    reference/user_agent

..  toctree::
    :maxdepth: 2

    reference/bozo
    reference/bozo_exception
    reference/etag
    reference/feeds
    reference/headers
    reference/href
    reference/lists
    reference/meta
    reference/modified
    reference/modified_parsed
    reference/opportunities
    reference/status
    reference/version
